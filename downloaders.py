"""
This downloaders module is home to the abstract statement downloader
and any comcrete implementations of it. If you have a bank site you
want to download a statement from, create a concrete implementation of
the statement downloader here and then add an entry to your settings file.
"""

import abc
import logging
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.common.exceptions import (TimeoutException, 
                                        ElementNotVisibleException,
                                        NoSuchElementException, 
                                        StaleElementReferenceException)


logging.basicConfig(level=logging.INFO)
log = logging.getLogger("downloader")


class StatementDownloader(metaclass=abc.ABCMeta):
    """
    This class serves as a template for all statement downloaders.
    If you have a bank website you want to download a statement
    from, subclass this and implement the abstract methods.
    """

    driver = None
    website = ""
    username = ""
    password = ""

    def __init__(self, driver, username, password):
        self.driver = driver
        self.username = username
        self.password = password

    @abc.abstractmethod
    def login(self):
        pass

    @abc.abstractmethod
    def go_to_statements_page(self):
        pass
    
    @abc.abstractmethod
    def download_most_recent_statement(self):
        pass


class WellsFargoStatementDownloader(StatementDownloader):
    """
    Class for downloading statements from Wells Fargo. You will note that
    several of the methods perform groups of actions surrounded by a
    try/except and backoff. TLDR: Wells Fargo's site displays elements
    before they are ready to be interacted with. Selenium struggles with
    this. One way around is to perform a series of actions and keep trying
    them until they ALL succeed. If you knew more about the internals of the
    site there are other more elegant options you could pursue.
    """

    website = "http://www.wellsfargo.com"

    def login(self):
        log.info("Opening %s and loging in", self.website)
        self.driver.get(self.website)
        self.driver.find_element_by_id("userid").send_keys(self.username)
        self.driver.find_element_by_id("password").send_keys(self.password)
        self.driver.find_element_by_id("btnSignon").click()

        # Wait for up to 10 seconds for the COMPLETE ADVANTAGE link to 
        # appear which signals that the Account Summary page is open.
        log.info("Waiting for account summary page to load")
        wait = WebDriverWait(self.driver, 10)
        wait_settings = (By.PARTIAL_LINK_TEXT, "COMPLETE ADVANTAGE")
        wait.until(EC.element_to_be_clickable(wait_settings))

    def go_to_statements_page(self):
        log.info("Navigating to the Statements page")
        for attempt in range(10):
            try:
                self.driver.find_element_by_partial_link_text("More").click()
                self.driver.find_element_by_link_text("Accounts and Settings").click()
                self.driver.find_element_by_link_text("Statements and Documents").click()
                break
            except ElementNotVisibleException:
                time.sleep(0.25)

        log.info("Expanding the Statements section")
        for attempt in range(10):
            try:
                self.driver.implicitly_wait(0)
                # Expand statements sections
                self.driver.find_element_by_id("stmtdisc").click()
                self.driver.find_element_by_css_selector("#stmtdisc.open")
                statement_link = self.driver.find_elements_by_class_name("document-title")
                break
            except (NoSuchElementException, StaleElementReferenceException):
                time.sleep(0.25)

    def download_most_recent_statement(self):
        log.info("Initiate download of the most recent statement")
        self.driver.implicitly_wait(10)
        for attempt in range(10):
            try:
                statement_link = self.driver.find_elements_by_class_name('document-title')[0]
                statement_link.click()
                break
            except (NoSuchElementException, 
                    StaleElementReferenceException, 
                    ElementNotVisibleException):
                time.sleep(0.25)
