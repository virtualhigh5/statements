"""
IMPORTANT: this script will only work if you have 
a Wells Fargo Online account and a Complete Advantage
checking account.

This script uses Selenium to log into your bank 
websites and download your statements. Before
executing, set up a settings.json file.

Execution Example: 
    
    python main.py

If you want to add another downloader to the mix, check out
the downloaders module and your settings.json file.

NOTE: The primary documentation should be self documenting 
method names. Logging statements are placed strategically to 
add additional documentation along with friendly output while
the script runs. Inline docs are place to explain things where
logs can't. Finally, method/class/module string docs like
this contain supplemental information where necessary.
"""

# Python libs
import json
import os
import time
import tempfile
import shutil
import sys
import logging
from datetime import datetime

# Third party libs
from selenium import webdriver

# Local libs
import downloaders


logging.basicConfig(level=logging.INFO)
log = logging.getLogger("main")


def main():
    settings = load_settings()
    try:
        log.info("Creating webdriver")
        temp_download_dir = tempfile.mkdtemp()
        driver = create_webdriver(temp_download_dir)

        log.info("Processing all downloaders in settings file")
        for downloader in settings["downloaders"]:
            downloader_class_name = downloader["class"]
            username = downloader["username"]
            password = downloader["password"]
            statements_dir = downloader["dest_dir"]

            if hasattr(downloaders, downloader_class_name):
                log.info("Creating downloader: %s", downloader_class_name)
                downloader_class = getattr(downloaders, downloader_class_name)
                downloader = downloader_class(driver, username, password)
            else:
                log.error("Sepcified downloader not found: %s", downloader_class_name)
                continue

            log.info("Executing downloader routines")
            downloader.login()
            downloader.go_to_statements_page()
            downloader.download_most_recent_statement()

            log.info("Renaming download and moving to final destination")
            statement_path = get_statement_temp_path(temp_download_dir)
            statement_name = datetime.now().strftime("%Y-%m Wells Fargo.pdf")
            new_statement_path = os.path.join(statements_dir, statement_name)
            os.rename(statement_path, new_statement_path)
            log.info("Statement available here: %s", new_statement_path)
    finally:
        log.info("Cleaning up temp dirs and shutting down driver")
        shutil.rmtree(temp_download_dir)
        driver.quit()


def load_settings():
    """
    Load program settings from a JSON file named settings.json.
    An example of this file is settings.json.dist.
    """
    log.info("Loading settings from settings.json")
    try:
        with open("settings.json") as settings:    
            settings = json.load(settings)
    except FileNotFoundError:
        log.error("Please create a settings.json file from settings.json.dist "
                  "and place it in the same directory as this script")
        exit(1)
    return settings


def create_webdriver(download_dir):
    """
    This will launch browser a browser configured to 
    download files to the specified location. This only 
    supports FireFox right now.

    :param download_dir: 
        The path to download files to.
    """
    # The Profile is used to configure the webdriver with particular settings.
    fp = webdriver.FirefoxProfile()
    fp.set_preference("browser.download.folderList",2)
    fp.set_preference("browser.download.manager.showWhenStarting", False)
    fp.set_preference("browser.download.dir", download_dir)
    fp.set_preference("browser.helperApps.neverAsk.saveToDisk","application/pdf")
    fp.set_preference("pdfjs.disabled", True)

    # Create a new instance of the Firefox driver
    driver = webdriver.Firefox(firefox_profile=fp)
    driver.implicitly_wait(10)
    return driver


def get_statement_temp_path(download_dir, delay=0.1):
    """
    Wait for the statement to complete downloading and return
    the full path to it once it is done. The strategy used here
    to determine when a file has completed downloading is to
    periodically check the file size. When the file size stops
    changing, we consider it done.

    :param download_dir:
        The directory where the statement is being downloaded to.
    :param delay:
        The amount of time to wait (secs) before retrying operations.
    """
    log.info("Finding downloaded statement")
    statement_path = None
    for attempt in range(10):
        dir_list = os.listdir(download_dir)
        if len(dir_list) > 0:
            log.info("Download found, waiting for completion")
            statement_name = dir_list[0] # only 1 file in the tmp dir
            statement_path = os.path.join(download_dir, statement_name)
            while True:
                if not os.path.isfile(statement_path):
                    time.sleep(delay)
                    continue
                statement_size = os.path.getsize(statement_path)
                log.info("Bytes Downloaded: " + str(statement_size))
                time.sleep(delay)
                if statement_size == os.path.getsize(statement_path):
                    log.info("Download has completed")
                    break
                log.info("File is still downloading...")
        if statement_path:
            break
        time.sleep(delay)
    return statement_path


if __name__ == "__main__":
    main()
